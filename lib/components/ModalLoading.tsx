import React from "react";
import {
  View,
  Modal,
  ModalProps,
  ActivityIndicator,
  StyleSheet,
  Platform,
} from "react-native";

import { Colors } from "../../utils/Colors";

import { Text } from ".";

export type ModalLoadingProps = {
  mensagem: String;
};

export const ModalLoading = (props: ModalLoadingProps | ModalProps) => {
  const { mensagem, ...modalProps } = props;
  return (
    <Modal transparent={true} animationType={"none"} {...modalProps}>
      <View style={Styles.container}>
        <View style={[Styles.containerModal, Styles.shadow]}>
          <Text>{mensagem}</Text>
          <ActivityIndicator
            style={{ marginTop: 15 }}
            color={Colors.blue.default}
            size="large"
          />
        </View>
      </View>
    </Modal>
  );
};

export const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#00000040",
  },
  containerModal: {
    backgroundColor: Colors.white.default,
    borderRadius: 13,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  shadow: {
    elevation: 3,
    borderBottomWidth: Platform.select({
      ios: 1,
      android: 0,
    }),

    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: Platform.select({
      ios: 0,
      android: 0.38,
    }),
    shadowRadius: Platform.select({
      ios: 0,
      android: 3.5,
    }),
  },
});
