import React, { useEffect, useState } from "react";
import { Colors } from "../../utils/Colors";
import { StyleSheet, Text, View } from "react-native";

import IconeChecked from "../../assets/icons/svg/checked.svg";
import IconeClosed from "../../assets/icons/svg/close.svg";
import IconeMoney from "../../assets/icons/svg/money.svg";
import IconeTime from "../../assets/icons/svg/time.svg";

import { global_constants } from "../../config/global_constants";

type BarraProgressoProps = {
  statudId: number;
  statusDesc: string;
};

const {
  Enum: { StatusCupomSolicitacao },
} = global_constants;

const idToProgress = {
  [StatusCupomSolicitacao[0].id]: 0,
  [StatusCupomSolicitacao[2].id]: 50,
  [StatusCupomSolicitacao[11].id]: 50,
  [StatusCupomSolicitacao[1].id]: 100,
  [StatusCupomSolicitacao[10].id]: 100,
};

const mapStatusIdToProgress = (statusId: number) =>
  new Promise((resolve, reject) =>
    idToProgress[statusId] ? resolve(idToProgress[statusId]) : reject(statusId)
  );

export function BarraProgresso(props: BarraProgressoProps) {
  const { statusId, statusDesc } = props;
  const [progresso, inserirProgresso] = useState(0);
  const [error, inserirError] = useState(false); // flag que dispara o erro e altera o estilo do progresso
  const [motivo, inserirMotivo] = useState("");

  useEffect(() => {
    mapStatusIdToProgress(statusId)
      .then(inserirProgresso)
      .catch(() => {
        inserirError(true);
        inserirMotivo(statusDesc ?? "");
      });
  }, []);

  return (
    <View
      style={{
        borderBottomWidth: 0.7,
        borderBottomColor: Colors.grey.alt2,
        flexDirection: "column",
        paddingTop: 20,
        paddingBottom: 20,
      }}
    >
      <View style={progressoStyles.containerProgresso}>
        <View style={progressoStyles.progressoBackground} />

        <View
          style={[
            progressoStyles.progressoNormal,
            {
              backgroundColor: error
                ? Colors.grey.alt14
                : progresso == 100
                ? Colors.blue.alt2
                : Colors.blue.alt5,
              marginLeft: progresso == 100 ? -10 : 0,
              width: error ? 50 + "%" : progresso == 0 ? 30 : progresso + "%",
            },
          ]}
        />

        <View
          style={[
            progressoStyles.iconeProgressoNormal,
            {
              backgroundColor: error
                ? Colors.grey.alt14
                : progresso == 100
                ? Colors.blue.alt2
                : Colors.blue.alt5,
            },
          ]}
        >
          {error ? <IconeClosed width={16} height={16} /> : null}

          {progresso < 50 && error != true ? (
            <IconeMoney width={20} height={20} />
          ) : null}

          {progresso >= 50 && progresso <= 99 && error != true ? (
            <IconeTime width={20} height={20} />
          ) : null}

          {progresso == 100 && error != true ? (
            <IconeChecked width={20} height={20} />
          ) : null}
        </View>
      </View>

      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        {error ? (
          <View style={progressoStyles.statusContainer}>
            <Text style={progressoStyles.statusCupom}>Cupom inválido</Text>
            <Text style={progressoStyles.statusDescricao}>{motivo}</Text>
          </View>
        ) : (
          <>
            {progresso < 50 ? (
              <Text style={progressoStyles.statusCupom}>Aguardando envio</Text>
            ) : null}

            {progresso >= 50 && progresso < 100 ? (
              <Text style={progressoStyles.statusCupom}>Em processamento</Text>
            ) : null}

            {progresso == 100 ? (
              <>
                <Text style={progressoStyles.statusCupom}>Cupom válido</Text>
                <Text style={progressoStyles.statusDescricao}>
                  CPF no aplicativo
                </Text>
              </>
            ) : null}
          </>
        )}
      </View>
    </View>
  );
}

const progressoStyles = StyleSheet.create({
  containerProgresso: {
    alignItems: "center",
    flexDirection: "row",
    height: 0,
    borderRadius: 30,
    marginTop: 40,
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 20,
    padding: 3,
  },
  progressoBackground: {
    backgroundColor: Colors.grey.alt3,
    borderRadius: 16,
    height: 18,
    width: "100%",
    position: "absolute",
    zIndex: 1,
  },
  progressoNormal: {
    borderRadius: 16,
    height: 18,
    position: "relative",
    width: "100%",
    zIndex: 2,
  },
  iconeProgressoNormal: {
    alignItems: "center",
    borderRadius: 100,
    backgroundColor: Colors.blue.alt2,
    height: 35,
    justifyContent: "center",
    marginLeft: -20,
    position: "relative",
    width: 35,
    zIndex: 3,
  },
  statusContainer: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
  statusCupom: {
    color: Colors.grey.alt1,
    fontFamily: "Bariol-Regular",
    fontSize: 20,
    position: "relative",
  },
  statusDescricao: {
    color: Colors.grey.alt,
    fontFamily: "Bariol-Regular",
    fontSize: 12,
    position: "relative",
    textAlign: "center",
    width: 350,
  },
});
